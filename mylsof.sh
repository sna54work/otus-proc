#!/bin/bash

if [[ -z $1 ]]; then
  echo "Please specify a file or directory as an argument"
  exit 1
fi

# Find all processes using lsof, and extract their PIDs
pid_list=$(fuser -v $1 2>/dev/null | awk '{print $1}' | sort -u)

if [[ -z $pid_list ]]; then
  echo "No processes found with $1 open"
  exit 0
fi

# Get the process details using ps
ps -p $pid_list -o pid,user,command
