#!/bin/bash

if [[ $# -eq 0 ]]; then
  echo "Usage: $0 file_or_directory_path" >&2
  exit 1
fi

if [[ -e $1 ]]; then
  if [[ -f $1 ]]; then
    # file
    echo "lsof for file $1:"
    for pid in $(ls -1 /proc | grep '^[0-9]*$'); do
      if [[ -e /proc/$pid/fd ]]; then
        for fd in /proc/$pid/fd/*; do
          if [[ "$(readlink $fd)" == "$(realpath $1)" ]]; then
            echo "Process $pid has $1 opened"
            ps -p $pid -o pid,user,command
          fi
        done
      fi
    done
  elif [[ -d $1 ]]; then
    # directory
    echo "lsof for directory $1:"
    for pid in $(ls -1 /proc | grep '^[0-9]*$'); do
      if [[ -e /proc/$pid/fd ]]; then
        for fd in /proc/$pid/fd/*; do
          if [[ "$(readlink -f $fd)" == "$(realpath -s $1)"* ]]; then
            echo "Process $pid has files opened in $1"
            ps -p $pid -o pid,user,command
          fi
        done
      fi
    done
  else
    echo "$1 is not a valid file or directory" >&2
    exit 1
  fi
else
  echo "$1 does not exist" >&2
  exit 1
fi
